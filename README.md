Octave GitLab CI Example
========================

This repository shows an example of how to use [GitLab CI] to do continuous
integration with an [Octave][GNU Octave] project.

See https://github.com/mtmiller/octave-travis-ci-example for an equivalent
example using Travis CI.

The Docker image [mtmiller/octave] is maintained at
https://gitlab.com/mtmiller/docker-octave.

## About Octave

[GNU Octave] is a high-level interpreted language, primarily intended for
numerical computations. It provides capabilities for the numerical solution of
linear and nonlinear problems, and for performing other numerical experiments.
It also provides extensive graphics capabilities for data visualization and
manipulation. Octave is normally used through its interactive command line
interface, but it can also be used to write non-interactive programs. The
Octave language is quite similar to Matlab so that most programs are easily
portable.

## Get Involved

If you want to contribute to this example project, please read the
[contribution guidelines](CONTRIBUTING.md).

## License

The scripts making up this example are licensed under a modified BSD license.
See [LICENSE.md](LICENSE.md) for the full license text.

[GNU Octave] is free software: you can redistribute it and/or modify it under
the terms of the [GNU General Public License][gpl], either version 3 or any
later version.

[GNU Octave]: https://www.octave.org/
[GitLab CI]: https://about.gitlab.com/product/continuous-integration/
[gpl]: https://www.gnu.org/licenses/gpl-3.0.html
[mtmiller/octave]: https://hub.docker.com/r/mtmiller/octave
